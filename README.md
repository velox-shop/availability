# Work locally

Build Service

```
./gradlew clean build
```

Execute with Gradle:

```
./gradlew bootRun --args='--spring.profiles.active=embeddeddb,localauth'
```

Execute with Docker Compose

```
docker-compose stop && docker-compose rm -fv && docker-compose up --build --force-recreate --remove-orphans
```

Test

```
for file in src/test/*postman_collection.json; do   newman run $file --environment src/test/veloxAvailability-local.postman_environment.json --reporters cli,html --reporter-html-export "newman-results-$(basename $file .json).html"; done
```

## API documentation

Online API Documentation is at:

- HTML: http://localhost:8445/availability/v1/swagger-ui.html
- JSON: http://localhost:8445/availability/v1/api-docs

Offline API Documentation is at https://velox-shop.gitlab.io/availability/

