package shop.velox.availability.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import jakarta.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.velox.availability.model.AvailabilityEntity;
import shop.velox.availability.model.AvailabilityStatus;

class AvailabilityRepositoryIntegrationTest extends AbstractJpaTest {

  @Resource
  private AvailabilityRepository availabilityRepository;

  @Test
  @Order(1)
  void auditTest() {
    AvailabilityEntity savedEntity = availabilityRepository
        .save(AvailabilityEntity.builder()
            .articleId("articleId")
            .quantity(BigDecimal.valueOf(123.45))
            .status(AvailabilityStatus.IN_STOCK)
            .build());

    assertNotNull(savedEntity.getCreateTime());
    assertNotNull(savedEntity.getModifiedTime());
  }

  @Test
  @Order(2)
  void TotalNumberOfAvailabilitiesTest() {

    availabilityRepository.save(AvailabilityEntity.builder()
        .articleId("articleId")
        .quantity(BigDecimal.valueOf(123.45))
        .status(AvailabilityStatus.IN_STOCK)
        .build());
    assertEquals(1L, availabilityRepository.findAll(Pageable.unpaged()).getTotalElements());
  }

  @Test
  @Order(3)
  void findAllByFiltersTest() {
    // If
    var articleId = "articleId";
    availabilityRepository.save(AvailabilityEntity.builder()
        .articleId(articleId)
        .quantity(BigDecimal.valueOf(0))
        .status(AvailabilityStatus.NOT_AVAILABLE)
        .build());

    availabilityRepository.save(
        AvailabilityEntity.builder()
            .articleId(articleId)
            .quantity(BigDecimal.valueOf(3))
            .status(AvailabilityStatus.IN_STOCK)
            .build());

    availabilityRepository.save(AvailabilityEntity.builder()
        .articleId("otherArticleId")
        .quantity(BigDecimal.valueOf(123.45))
        .status(AvailabilityStatus.IN_STOCK)
        .build());

    // When
    Page<AvailabilityEntity> results = availabilityRepository
        .findAllByArticleIdIn(List.of(articleId, "unknownArticleId"), Pageable.unpaged());

    // Then
    assertEquals(2, results.getContent().size());
    assertEquals(articleId, results.getContent().get(0).getArticleId());
    assertEquals(articleId, results.getContent().get(1).getArticleId());
  }

  @Test
  @Order(4)
  void findByIdTest() {
    AvailabilityEntity existing = availabilityRepository.save(
        AvailabilityEntity.builder()
            .articleId("articleId")
            .quantity(BigDecimal.valueOf(123.45))
            .status(AvailabilityStatus.IN_STOCK)
            .build());
    assertEquals(1L, availabilityRepository.findAll(Pageable.unpaged()).getTotalElements());

    assertThat(availabilityRepository.findOneById(existing.getId())).isNotEmpty();
    assertThat(availabilityRepository.findOneByArticleId(existing.getArticleId())).isNotEmpty();
  }
}
