#!/bin/sh
echo -n "waiting for docker:$1 - "
ncr=1; i="0"; while [ $i -lt 60 -a "$ncr" != "0" ]; do echo -n "$i,"; i=`expr $i + 1`; sleep 1; nc -z docker $1 ;ncr=$?; done
if [ $ncr -eq 0 ]
then
  echo ""
  echo -n "result: sucess, "
else
  echo ""
  echo -n "result: error, "
fi
echo "code:$ncr, tries:$i"
exit $ncr