package shop.velox.availability.converter;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.availability.api.dto.AvailabilityDto;
import shop.velox.availability.model.AvailabilityEntity;
import shop.velox.commons.converter.Converter;

@Mapper
public interface AvailabilityConverter extends Converter<AvailabilityEntity, AvailabilityDto> {

  @Override
  @Mappings({
      @Mapping(target = "id", ignore = true)
  })
  AvailabilityEntity convertDtoToEntity(AvailabilityDto availabilityDto);
}
