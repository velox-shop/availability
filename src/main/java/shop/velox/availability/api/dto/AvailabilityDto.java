package shop.velox.availability.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.availability.model.AvailabilityStatus;


@Data
@Builder
@Jacksonized
@FieldNameConstants
public class AvailabilityDto {

  @Schema(description = "Unique identifier of the Availability.", example = "5dc618af-af49-4adc-bccd-4d17aeff7526")
  private String id;

  @Schema(description = "Unique identifier of the article.", example = "123")
  @Size(min = 3, max = 50)
  @NotNull
  private String articleId;

  @Schema(description = "Non-negative available quantity of the article.", example = "10")
  @PositiveOrZero
  private BigDecimal quantity;

  @Schema(description = "Availability stock status of the article.", example = "IN_STOCK")
  @Valid
  private AvailabilityStatus status;

  @Schema(description = "Standard lead time in days for article quantity replenishment.", example = "14", defaultValue = "0")
  @PositiveOrZero
  private int replenishmentTime;

}
