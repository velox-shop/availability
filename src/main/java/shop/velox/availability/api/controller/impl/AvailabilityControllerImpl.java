package shop.velox.availability.api.controller.impl;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.availability.api.controller.AvailabilityController;
import shop.velox.availability.api.dto.AvailabilityDto;
import shop.velox.availability.model.AvailabilityEntity;
import shop.velox.availability.service.AvailabilityService;
import shop.velox.commons.converter.Converter;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AvailabilityControllerImpl implements AvailabilityController {

  private final AvailabilityService availabilityService;

  private final Converter<AvailabilityEntity, AvailabilityDto> availabilityConverter;

  @Override
  public ResponseEntity<AvailabilityDto> createAvailability(final AvailabilityDto availabilityDto) {
    log.info("createAvailability: {}", availabilityDto);
    AvailabilityDto body = availabilityConverter
        .convertEntityToDto(availabilityService
            .createAvailability(availabilityConverter.convertDtoToEntity(availabilityDto)));
    return ResponseEntity.status(HttpStatus.CREATED).body(body);
  }

  @Override
  public ResponseEntity<AvailabilityDto> getAvailability(final String id) {
    AvailabilityEntity availabilityEntity = availabilityService.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    AvailabilityDto availabilityDto = availabilityConverter.convertEntityToDto(availabilityEntity);
    return ResponseEntity.ok(availabilityDto);
  }

  @Override
  public ResponseEntity<Page<AvailabilityDto>> getAvailabilities(Pageable pageable,
      final List<String> articleIds) {

    log.info("getAvailabilities with articleIds: {}", String.join(", ", emptyIfNull(articleIds)));

    Page<AvailabilityEntity> entitiesPage;
    Pageable responsePageable;
    if (isEmpty(articleIds)) {
      responsePageable = pageable;
      entitiesPage = availabilityService.findAll(responsePageable);
    } else {
      responsePageable = PageRequest.of(0, articleIds.size());
      entitiesPage = availabilityService.findAllByArticleIds(responsePageable, articleIds);
    }
    Page<AvailabilityDto> dtosPage = availabilityConverter.convert(responsePageable, entitiesPage);
    return ResponseEntity.ok(dtosPage);
  }

  @Override
  public ResponseEntity<AvailabilityDto> updateAvailability(final String id,
      final AvailabilityDto availabilityDto) {
    log.info("updateAvailability {}, {}", id, availabilityDto);
    AvailabilityEntity availabilityEntity = availabilityService
        .updateAvailability(id, availabilityConverter.convertDtoToEntity(
            availabilityDto));
    return ResponseEntity.ok(availabilityConverter.convertEntityToDto(availabilityEntity));
  }

  @Override
  public ResponseEntity<Void> deleteAvailability(final String id) {
    log.info("deleteAvailability {}", id);
    availabilityService.deleteAvailability(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    return ex.getBindingResult()
        .getAllErrors()
        .stream()
        .collect(Collectors.toMap(
            error -> ((FieldError) error).getField(),
            error -> defaultIfEmpty(EMPTY, error.getDefaultMessage())));
  }
}
