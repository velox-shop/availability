package shop.velox.availability;

import javax.sql.DataSource;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(
    scanBasePackages = {"shop.velox.availability", "shop.velox.commons"},
    exclude = {SecurityAutoConfiguration.class}
)
@EnableRetry
@EnableJpaAuditing
@EnableJpaRepositories(basePackages = {"shop.velox.availability.dao"})
@EnableTransactionManagement
public class AvailabilityApplication {

  public static void main(String[] args) {
    SpringApplication.run(AvailabilityApplication.class, args);
  }

  @Bean
  public BeanPostProcessor dataSouceWrapper() {
    return new RetryableDataSourceBeanPostProcessor();
  }

  @Order(Ordered.HIGHEST_PRECEDENCE)
  private class RetryableDataSourceBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)
        throws BeansException {
      if (bean instanceof DataSource) {
        bean = new RetryableDataSource((DataSource) bean);
      }
      return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName)
        throws BeansException {
      return bean;
    }
  }

}
