package shop.velox.availability.dao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import shop.velox.availability.model.AvailabilityEntity;

public interface AvailabilityRepository extends JpaRepository<AvailabilityEntity, String> {

  Optional<AvailabilityEntity> findOneById(String id);

  Optional<AvailabilityEntity> findOneByArticleId(String articleId);

  Page<AvailabilityEntity> findAllByArticleIdIn(List<String> articleId, Pageable pageable);

}