package shop.velox.availability.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.availability.model.AvailabilityEntity;

/**
 * Executes CRUD operations on Availability
 */
public interface AvailabilityService {

  /**
   * Creates an availability and returns it
   *
   * @param availabilityEntity the availability to be created
   * @return the just created availability
   */
  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
      + "T(shop.velox.availability.service.AvailabilityServiceConstants.Authorities).AVAILABILITY_ADMIN)")
  AvailabilityEntity createAvailability(AvailabilityEntity availabilityEntity);


  /**
   * Gets an availability by its id
   *
   * @param id the id of the availability
   * @return the availability wrapped in a {@link Optional}
   */
  Optional<AvailabilityEntity> findById(String id);


  /**
   * Gets all availabilities
   *
   * @param pageable  the structure of the returned page
   * @return the availabilities wrapped in a {@link Page}
   */
  Page<AvailabilityEntity> findAll(Pageable pageable);

  /**
   * Gets availabilities matching any of articleIds
   *
   * @param articleIds the ids of the articles
   * @param pageable  the structure of the returned page
   * @return the availabilities wrapped in a {@link Page}
   */
  Page<AvailabilityEntity> findAllByArticleIds(Pageable pageable, List<String> articleIds);


  /**
   * Updates an availability with the given id
   *
   * @param id                 the id of the availability to be updated
   * @param availabilityEntity contains the new data for the availability to be updated
   * @return the updated availability
   */
  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
      + "T(shop.velox.availability.service.AvailabilityServiceConstants.Authorities).AVAILABILITY_ADMIN)")
  AvailabilityEntity updateAvailability(String id, AvailabilityEntity availabilityEntity);


  /**
   * Removes an availability with given id
   *
   * @param id the id of the availability to be removed
   * @return the status if the availability is deleted
   */
  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
      + "T(shop.velox.availability.service.AvailabilityServiceConstants.Authorities).AVAILABILITY_ADMIN)")
  void deleteAvailability(String id);
}
