package shop.velox.availability.service.impl;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.availability.dao.AvailabilityRepository;
import shop.velox.availability.model.AvailabilityEntity;
import shop.velox.availability.service.AvailabilityService;

@Service
@RequiredArgsConstructor
@Slf4j
public class AvailabilityServiceImpl implements AvailabilityService {

  private final AvailabilityRepository availabilityRepository;

  @Override
  public AvailabilityEntity createAvailability(final AvailabilityEntity availabilityEntity) {
    if (StringUtils.isBlank(availabilityEntity.getArticleId())) {
      log.info("cannot create availability without articleId: {}",
          availabilityEntity);
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "articleId missing");
    }

    Optional<AvailabilityEntity> existingAvailability = availabilityRepository
        .findOneByArticleId(availabilityEntity.getArticleId());

    if (existingAvailability.isPresent()) {
      log.info(
          "cannot create availability for article with enclosed articleId {}, it already exists",
          availabilityEntity);
      throw new ResponseStatusException(HttpStatus.CONFLICT,
          "Availability for article with id: " + availabilityEntity.getArticleId() +
              " already exists");
    }

    AvailabilityEntity createdAvailabilityEntity = availabilityRepository
        .saveAndFlush(availabilityEntity);
    log.info("Created availability {}", availabilityEntity);
    return createdAvailabilityEntity;
  }

  @Override
  public Optional<AvailabilityEntity> findById(String id) {
    return availabilityRepository.findOneById(id);
  }

  @Override
  public Page<AvailabilityEntity> findAll(Pageable pageable) {
    return availabilityRepository.findAll(pageable);
  }

  @Override
  public Page<AvailabilityEntity> findAllByArticleIds(Pageable pageable,
      List<String> articleIds) {
    if (isEmpty(articleIds)) {
      return availabilityRepository.findAll(pageable);
    }
    return availabilityRepository.findAllByArticleIdIn(articleIds, pageable);
  }

  @Override
  public AvailabilityEntity updateAvailability(String id, AvailabilityEntity availabilityEntity) {

    log.info("Update {} with: {}", id, availabilityEntity);

    if (StringUtils.isNotBlank(availabilityEntity.getId()) && !StringUtils
        .equals(id, availabilityEntity.getId())) {
      throw new ResponseStatusException(HttpStatus.CONFLICT,
          "Cannot manually change the availability id");
    }

    AvailabilityEntity existingAvailability = availabilityRepository.findOneById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    if (!StringUtils
        .equals(existingAvailability.getArticleId(), availabilityEntity.getArticleId())) {
      throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot update the articleId");
    }

    if (availabilityEntity.getQuantity() != null) {
      existingAvailability.setQuantity(availabilityEntity.getQuantity());
    }
    if (availabilityEntity.getStatus() != null) {
      existingAvailability.setStatus(availabilityEntity.getStatus());
    }

    if (availabilityEntity.getReplenishmentTime() != existingAvailability.getReplenishmentTime()) {
      existingAvailability.setReplenishmentTime(availabilityEntity.getReplenishmentTime());
    }

    return availabilityRepository.saveAndFlush(existingAvailability);
  }

  @Override
  @Transactional
  public void deleteAvailability(final String id) {

    log.info("removeAvailability {}", id);

    if (StringUtils.isBlank(id)) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
          "Cannot remove availability without id");
    }

    AvailabilityEntity entity = availabilityRepository.findOneById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    availabilityRepository.delete(entity);
  }
}
