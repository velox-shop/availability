package shop.velox.availability.service;

public final class AvailabilityServiceConstants {

  public static class Authorities {

    public static final String AVAILABILITY_ADMIN = "Admin_Availability";
  }

  private AvailabilityServiceConstants() {
    // private constructor to prevent instantiation
  }
}
