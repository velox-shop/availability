package shop.velox.availability.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import org.apache.commons.lang3.StringUtils;
import shop.velox.commons.model.AbstractEntity;


@Entity
@Table(name = "AVAILABILITIES")
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@Jacksonized
@NoArgsConstructor
@FieldNameConstants
public class AvailabilityEntity extends AbstractEntity {
  
  @PrePersist
  private void prePersistFunction() {
    if (StringUtils.isEmpty(id)) {
      id = UUID.randomUUID().toString();
    }
  }

  @Column(unique = true, nullable = false)
  private String id;

  @Column(nullable = false)
  private String articleId;

  @Column(nullable = false)
  private BigDecimal quantity;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private AvailabilityStatus status;

  @Column(nullable = false)
  private int replenishmentTime;
}
